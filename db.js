
var db;

export function getDb() {
	return db ||= initDb();
}

function initDb() {
	return new Promise((resolve, reject) => {
		var request = indexedDB.open("database");
		request.onerror = () => reject(request.error);
		request.onsuccess = event => {
			console.debug("onsuccess");
			resolve(event.target.result);
		};
		request.onupgradeneeded = event => {
			console.debug("onupgradeneeded");
			//request.onsuccess = null;
			var db = event.target.result;
			var objectStore = db.createObjectStore("images", { keyPath: "internalId", autoIncrement: true });
			objectStore.createIndex("currentId_emoji", "currentId_emoji", {unique: true});
			objectStore.createIndex("currentId_sticker", "currentId_sticker", {unique: true});
			db.createObjectStore("settings");
			chrome.storage.local.get().then(async storage => {
				console.debug("storage", storage);
				for (let emoji of storage.emojis) {
					try {
						emoji.data = await fetch(storage[`data-${emoji.internalId}`]).then(res => res.blob());
						await storeImage(emoji);
					} catch (error) {
						console.error(error);
					}
				}
				await setSetting("defaultAnimationStyle", storage.defaultAnimationStyle);
				//resolve(db);
				console.debug("finished upgrade");
			});
		};
		request.onversionchange = () => {
			db = undefined; // idk
		};
	});
}


export async function getKnownEmojiIds() {
	var db = await getDb();
	return await new Promise((resolve, reject) => {
		var request = db.transaction(["images"], "readonly").objectStore("images").index("currentId_emoji").getAllKeys();
		request.onsuccess = () => resolve(request.result);
		request.onerror = () => reject(request.error);
	});
}

export async function getKnownStickerIds() {
	var db = await getDb();
	return await new Promise((resolve, reject) => {
		var request = db.transaction(["images"], "readonly").objectStore("images").index("currentId_sticker").getAllKeys();
		request.onsuccess = () => resolve(request.result);
		request.onerror = () => reject(request.error);
	});
}

export async function getAllImages() {
	var db = await getDb();
	return await new Promise((resolve, reject) => {
		var request = db.transaction(["images"], "readonly").objectStore("images").getAll();
		request.onsuccess = () => resolve(request.result);
		request.onerror = () => reject(request.error);
	});
}

export async function storeImage(entry) {
	var db = await getDb();
	return await new Promise((resolve, reject) => {
		var request = db.transaction(["images"], "readwrite").objectStore("images").add(entry);
		request.onsuccess = () => resolve(request.result);
		request.onerror = () => reject(request.error);
	});
}

export async function getImage(internalId) {
	var db = await getDb();
	return await new Promise((resolve, reject) => {
		var request = db.transaction(["images"], "readonly").objectStore("images").get(internalId);
		request.onsuccess = () => resolve(request.result);
		request.onerror = () => reject(request.error);
	});
}

export async function updateImage(internalId, update) {
	var db = await getDb();
	return await new Promise((resolve, reject) => {
		var objectStore = db.transaction(["images"], "readwrite").objectStore("images");
		var request = objectStore.get(internalId);
		request.onsuccess = event => {
			var request = objectStore.put(Object.assign(event.target.result, update));
			request.onsuccess = () => resolve(request.result);
			request.onerror = () => reject(request.error);
		};
		request.onerror = () => reject(request.error);
	});
}

export async function deleteImage(internalId) {
	var db = await getDb();
	return await new Promise((resolve, reject) => {
		var request = db.transaction(["images"], "readwrite").objectStore("images").delete(internalId);
		request.onsuccess = () => resolve(request.result);
		request.onerror = () => reject(request.error);
	});
}

export async function deleteAllImages() {
	var db = await getDb();
	return await new Promise((resolve, reject) => {
		var request = db.transaction(["images"], "readwrite").objectStore("images").clear();
		request.onsuccess = () => resolve(request.result);
		request.onerror = () => reject(request.error);
	});
}

export async function setSetting(key, value) {
	var db = await getDb();
	return await new Promise((resolve, reject) => {
		var request = db.transaction(["settings"], "readwrite").objectStore("settings").put(value, key);
		request.onsuccess = () => resolve(request.result);
		request.onerror = () => reject(request.error);
	});
}

export async function getSetting(key) {
	var db = await getDb();
	return await new Promise((resolve, reject) => {
		var request = db.transaction(["settings"], "readonly").objectStore("settings").get(key);
		request.onsuccess = () => resolve(request.result);
		request.onerror = () => reject(request.error);
	});
}