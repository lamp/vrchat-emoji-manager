btn1.onclick = () => open("manage.html");
btn2.onclick = () => {
	chrome.windows.create({
		url: "manage.html",
		focused: true,
		type: "popup"
	});
};

var width = Math.max(btn1.clientWidth, btn2.clientWidth);
btn1.style.width = width + "px";
btn2.style.width = width + "px";