var functions = {
	async getFiles(tag = "emoji") {
		var res = await fetch(`https://vrchat.com/api/1/files?tag=${tag}&n=100&offset=0`);
		return {
			status: res.status,
			body: await res.json(),
			retryAfter: res.headers.get("Retry-After") || undefined
		};
	},
	async deleteFile(id) {
		var res = await fetch(`https://vrchat.com/api/1/file/${id}`, {method: "DELETE"});
		return {
			status: res.status,
			body: await res.json(),
			retryAfter: res.headers.get("Retry-After") || undefined
		};
	},
	async createFile({url, tag = "emoji", animationStyle, frames, framesOverTime}) {
		var blob = await fetch(url).then(res => res.blob());
		var form = new FormData();
		form.append("tag", tag);
		if (animationStyle) form.append("animationStyle", animationStyle);
		if (frames) form.append("frames", frames);
		if (framesOverTime) form.append("framesOverTime", framesOverTime);
		form.append("maskTag", "square");
		form.append("file", blob);
		var res = await fetch("https://vrchat.com/api/1/file/image", {
			method: "POST",
			body: form
		});
		return {
			status: res.status,
			body: await res.json(),
			retryAfter: res.headers.get("Retry-After") || undefined
		};
	}
};

functions.getFiles("emoji").then(data => {
	if (!data.error) chrome.runtime.sendMessage(["storeFiles", data.body, "emoji"]);
	else console.error(data.error);
});
functions.getFiles("sticker").then(data => {
	if (!data.error) chrome.runtime.sendMessage(["storeFiles", data.body, "sticker"]);
	else console.error(data.error);
});

chrome.runtime.onMessage.addListener(function([method, ...args], sender, sendResponse) {
	console.debug(arguments);
	functions[method].apply(null, args)
		.then(response => sendResponse({response}))
		.catch(error => sendResponse({error: error.toString()}));
	return true;
});