# VRChat Emoji and Sticker Manager

VRChat Plus has a custom emoji and sticker feature but they are limited to 9 emojis or stickers and you cannot change the animation styles of the emojis. This Chrome extension allows you to have a much larger collection of emojis or stickers and conveniently toggle them on and off when needed.

Get it on the Chrome Web Store!!! https://chromewebstore.google.com/detail/vrchat-emoji-and-sticker/obmoelidfamikmdhgjeoacpmkfhohekb

![image](2023-10-26_20-50-20-123%20VRChat_Emoji_Manager_-_Google_Chrome.png)

## Manual install from source

1. If you have git, run `git clone https://gitea.moe/lamp/vrchat-emoji-manager.git`. If not, [download](https://gitea.moe/lamp/vrchat-emoji-manager/archive/main.zip) and extract the zip.
2. Navigate to [chrome://extensions/](chrome://extensions/)
3. Enable developer mode
4. Click "Load unpacked"
5. Select the cloned or unzipped folder
6. Open https://vrchat.com/home, existing emojis should be imported
7. Click extension icon to launch in tab or window

###  Update

1. Export your emojis/stickers so you have a backup
2. If you installed with git, run `git pull`. If not, download the zip again, and extract it to the SAME PATH
3. Go to [chrome://extensions/](chrome://extensions/) and reload the extension

Note, do not move or rename the extension folder, otherwise you have to re-add the extension to Chrome, which will change the extension ID, and your emojis will be gone. It is also a good idea to keep an export file as a backup.

IF ANY ISSUES REPORT [HERE](https://gitea.moe/lamp/vrchat-emoji-manager/issues?state=open)