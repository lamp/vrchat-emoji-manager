import { getDb, getKnownEmojiIds, getKnownStickerIds, storeImage } from "./db.js";

getDb();

var functions = {
	async storeFiles(files, type) {
		console.debug("storeFiles", files, type);
		var knownIds = type == "sticker" ? await getKnownStickerIds() : await getKnownEmojiIds();
		console.debug("knownIds", knownIds);
		for (let file of files) {
			if (knownIds.includes(file.id)) continue;
			console.log("store", file.id);
			let e = {
				date: file.versions[1].created_at,
				currentId_emoji: file.tags.includes("emoji") ? file.id : null,
				currentId_sticker: file.tags.includes("sticker") ? file.id : null,
				internalId: file.id,
				animationStyle: file.animationStyle,
				data: await fetch(file.versions[1].file.url).then(res => res.blob())
			};
			if (file.tags.includes("animated")) {
				e.animated = true;
				e.data_spritesheet = e.data; //todo, convert spritesheet back to gif???? or make spritesheet player??
				e.framecount = file.frames;
				e.fps = file.framesOverTime;
			}
			await storeImage(e);
		}
	}
};

chrome.runtime.onMessage.addListener(function([method, ...args], sender, sendResponse) {
	console.debug(arguments);
	functions[method].apply(null, args).then(sendResponse);
	return true;
});